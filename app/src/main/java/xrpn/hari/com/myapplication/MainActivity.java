package xrpn.hari.com.myapplication;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;

import xrpn.hari.com.myapplication.Mjob.MJobScheduler;

public class MainActivity extends AppCompatActivity {

    private static int JOB_ID=101;
    private JobScheduler jobScheduler;
    private JobInfo jobInfo;
    EditText edt_val;
    EditText edt_interval;
    Button start_service;
    Button stop_service;
    Button refresh;
    TextView msg_txt;
    FirebaseJobDispatcher dispatcher;
    SharedPreferences pref;
    JobInfo.Builder builder;
    ComponentName componentName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edt_interval=(EditText)findViewById(R.id.edit_interval);
        edt_val=(EditText)findViewById(R.id.edit_value);
        start_service=(Button)findViewById(R.id.button);
        stop_service=(Button)findViewById(R.id.button2);
        refresh=(Button)findViewById(R.id.btn_refresh);
        msg_txt=(TextView)findViewById(R.id.txt_val);
        //refresh=(Button)findViewById(R.id.btn_refresh);
//        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(getApplicationContext()));




        componentName=new ComponentName(getApplicationContext(), MJobScheduler.class);
        jobScheduler=(JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        pref = getApplicationContext().getSharedPreferences("pref_coin_101", 0);

        edt_interval.setText(""+(pref.getLong("mills",(180*1000))/60000));
        edt_val.setText(""+pref.getInt("diff",500));

 msg_txt.setText(pref.getString("last_val","Not Set"));
        start_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mills=Integer.parseInt(edt_interval.getText().toString());
                long millss=(mills*60)*1000;
                SharedPreferences.Editor editor = pref.edit();

                editor.putLong("mills",millss);
                int diff=Integer.parseInt( edt_val.getText().toString());
                editor.putInt("diff",diff);

                editor.commit();
                builder=new JobInfo.Builder(JOB_ID,componentName);
//                builder.setPeriodic(10000,2000);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    builder.setMinimumLatency(millss);

                } else {

                    builder.setPeriodic(millss);

                }

               // builder.setPeriodic(5000);
                builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
                builder.setPersisted(true);
                jobInfo=builder.build();


               int st= jobScheduler.schedule(jobInfo);
                Log.d(">>>>>>", "onClick: "+st);



            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                msg_txt.setText(pref.getString("last_val","Not Set"));
            }
        });

//        JobTrigger jbb=new JobTrigger();
        stop_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         jobScheduler.cancel(JOB_ID);
            }
        });
    }
}
