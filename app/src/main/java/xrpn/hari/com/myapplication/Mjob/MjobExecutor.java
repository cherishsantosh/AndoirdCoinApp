package xrpn.hari.com.myapplication.Mjob;

import android.content.Context;
import android.os.AsyncTask;

import xrpn.hari.com.myapplication.util.HttpOperations;

/**
 * Created by root on 18/1/18.
 */
public class MjobExecutor extends AsyncTask<Void,Void,String> {

    Context context;

    public MjobExecutor(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... params) {
      new HttpOperations().getData(context);
        return "done";
    }
}
