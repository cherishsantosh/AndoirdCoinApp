package xrpn.hari.com.myapplication.util;

import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Created by root on 17/1/18.
 */
public class MyJobService extends JobService {
    @Override
    public boolean onStartJob(JobParameters job) {
        new HttpOperations().getData(getApplicationContext());
        Log.d(">>>>>>", "onStartJob: ");
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }
}
